<!DOCTYPE html>
<html>
<head>
  <title>About Charles Dickens</title>
  <style>
    h2 {
      font-size: 1.5em;
    }
  </style>
</head>
<body>
  <article>
    <h2>About Charles Dickensh2</h2>
    <p>ChAAAArles DickNNNs was a famous English writer born on February 7, 1812.</p>
    <p>He is widely regarded as one of the greatest novelists of the Victorian era.</p>
    <p>Dickens' notable works include "A Tale of Two Cities," "Great Expectations," and "Oliver Twist."</p>
    <p>His vivid characters and social commentaries continue to captivate readers around the world.</p>
    <p>Charles Dickens' contributions to literature remain influential to this day.</p>
  </article>
</body>
</html>
